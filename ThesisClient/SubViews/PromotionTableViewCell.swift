//
//  PromotionTableViewCell.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 08/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var expirationDateLabel: UILabel!
    
    private var promotion:Promotion!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    public func setPromotion(_ promotion:Promotion){
        self.promotion = promotion
        
        self.descriptionLabel.text = self.promotion.description
        self.expirationDateLabel.text = self.promotion.expirationDate.description
    }

}
