// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Login: StoryboardType {
    internal static let storyboardName = "Login"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Login.self)

    internal static let addressViewController = SceneType<AddressViewController>(storyboard: Login.self, identifier: "AddressViewController")

    internal static let locationViewController = SceneType<LocationViewController>(storyboard: Login.self, identifier: "LocationViewController")

    internal static let loginViewController = SceneType<LoginViewController>(storyboard: Login.self, identifier: "LoginViewController")

    internal static let signupViewController = SceneType<SignupViewController>(storyboard: Login.self, identifier: "SignupViewController")
  }
  internal enum MainTabBar: StoryboardType {
    internal static let storyboardName = "MainTabBar"

    internal static let initialScene = InitialSceneType<UIKit.UITabBarController>(storyboard: MainTabBar.self)

    internal static let newPromotionViewController = SceneType<NewPromotionViewController>(storyboard: MainTabBar.self, identifier: "NewPromotionViewController")

    internal static let promotionsViewController = SceneType<PromotionsViewController>(storyboard: MainTabBar.self, identifier: "PromotionsViewController")

    internal static let settingsViewController = SceneType<SettingsViewController>(storyboard: MainTabBar.self, identifier: "SettingsViewController")
  }
  internal enum Opening: StoryboardType {
    internal static let storyboardName = "Opening"

    internal static let initialScene = InitialSceneType<OpeningViewController>(storyboard: Opening.self)

    internal static let openingViewController = SceneType<OpeningViewController>(storyboard: Opening.self, identifier: "OpeningViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

private final class BundleToken {}
