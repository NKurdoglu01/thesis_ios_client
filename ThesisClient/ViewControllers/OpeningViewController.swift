//
//  OpeningViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 04/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import CoreLocation
import GeoFire

class OpeningViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let minLat = 41.279533342078771
//        let minLong = 1.9859880073697689
//
//        let maxLat = 41.546992934133435
//        let maxLong = 2.2649850449110631
//        
//        for i in 101...1000{
//
//            Auth.auth().createUser(withEmail: "testStore"+i.description+"@gmail.com", password: "12345678") { authResult, error in
//                if error == nil{
//                    let ref = Database.database().reference()
//
//                    let latitude = Double.random(in: minLat...maxLat)
//                    let longitude = Double.random(in: minLong...maxLong)
//
//                    let location = CLLocation(latitude: latitude, longitude: longitude)
//                    let address = Address(location: location, description: AddressDescription(apartmentNo: i, area: "Rand area", city: "Barcelona", country: "Spain", street: "Street..."))
//
//                    GeoFire(firebaseRef: ref.child("locations")).setLocation(CLLocation(latitude: latitude, longitude: longitude) , forKey: authResult!.user.uid)
//                    ref.child("stores").child(authResult!.user.uid).setValue(
//                        ["name":"Test Store " + i.description,
//                         "address":["country":"Spain",
//                                    "city":"Barcelona",
//                                    "area":"Rand area",
//                                    "apartmentNo":i,
//                                    "street":"Street...",
//                                    "location":[latitude,
//                                                longitude],
//                                    "promotions":[]]])
//                }
//            }
//        }
    }
    
    private func deleteUser(index:Int = 1626){
        if index < 3000{
            Auth.auth().signIn(withEmail: "testStore"+index.description+"@gmail.com", password: "12345678") {authResult, error in
                
                if error == nil{
                    let ref = Database.database().reference()
                    if let auth = authResult {
                        ref.child("stores").child(auth.user.uid).removeValue()
                        ref.child("locations").child(auth.user.uid).removeValue()
                    }
                    
                    authResult?.user.delete(completion: { err in
                        print("email: " + "testStore"+index.description+"@gmail.com")
                        self.deleteUser(index: index+1)
                    })
                }else{
                    print("email err: " + "testStore"+index.description+"@gmail.com")
                    self.deleteUser(index: index+1)
                }
                
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if Session.isAutoLoginEnabled {

            Auth.auth().signIn(withEmail: Session.email, password: Session.password) {authDataResult, error in
                if let err = error{
                    print(err)

                }else{

                    Session.authUser = authDataResult?.user

                    Database.database().reference().child("stores").child(Session.authUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in

                        if let dict = snapshot.value as? Dictionary<String,AnyObject>{
                            let json = JSON(dict)

                            let name = json["name"].stringValue
                            let address = json["address"]

                            let aptNo = address["apartmentNo"].intValue
                            let area = address["area"].stringValue
                            let city = address["city"].stringValue
                            let country = address["country"].stringValue
                            let street = address["street"].stringValue

                            let location = address["location"].arrayValue
                            let latitude = location[0].doubleValue
                            let longitude = location[1].doubleValue

                            var promotionArray:[Promotion] = []
                            let promotions = json["promotions"]
                            for promotionBase in promotions {
                                let promotionId = promotionBase.0
                                let promotion = promotionBase.1

                                let description = promotion["description"].stringValue

                                let expires = promotion["expires"].doubleValue
                                let expirationDate = Date(timeIntervalSince1970: expires)
                                let type = promotion["description"].intValue

                                let promotionStruct = Promotion(id: promotionId, description: description, expirationDate: expirationDate, type: type)
                                promotionArray.append(promotionStruct)
                            }


                            let addressStruct = Address(location: CLLocation(latitude: latitude, longitude: longitude) , description: AddressDescription(apartmentNo: aptNo, area: area, city: city, country: country, street: street))

                            Store.initMainStore(name: name, promotions: promotionArray, address: addressStruct, user: Session.authUser!)

                            let mainTabBar = StoryboardScene.MainTabBar.initialScene.instantiate()
                            self.present(mainTabBar, animated: true, completion: nil)
                        }
                    })
                }
            }
        }else{
            let loginVC = StoryboardScene.Login.initialScene.instantiate()
            self.present(loginVC, animated: true, completion: nil)
        }
    }
}
