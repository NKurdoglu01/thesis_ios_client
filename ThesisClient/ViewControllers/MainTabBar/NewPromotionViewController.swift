//
//  NewPromotionViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 29/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class NewPromotionViewController: UIViewController {

    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var typeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var expirationDatePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "New Promotion"
        
        self.hideKeyboardWhenTappedAround()

        self.descriptionTextField.layer.borderWidth = 1
        self.descriptionTextField.layer.borderColor = UIColor.black.cgColor
        
        let remoteConfig = RemoteConfig.remoteConfig()
        let json = JSON(parseJSON: remoteConfig["promotion_types"].stringValue!)

        self.typeSegmentedControl.removeAllSegments()
        for typeName in json["types"].arrayValue {
            self.typeSegmentedControl.insertSegment(withTitle: typeName.stringValue, at: self.typeSegmentedControl.numberOfSegments, animated: false)
        }
        self.typeSegmentedControl.selectedSegmentIndex = 0
    }
    
    @IBAction func createAction(_ sender: Any) {
        
        let dict:[String:Any] = ["description":self.descriptionTextField.text,
                    "expires":self.expirationDatePicker.date.timeIntervalSince1970,
                    "type":self.typeSegmentedControl.selectedSegmentIndex]
        
        Database.database().reference().child("stores").child(Session.authUser!.uid).child("promotions").childByAutoId().setValue(dict)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
