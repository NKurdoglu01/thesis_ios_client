//
//  PromotionsViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 08/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class PromotionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        Database.database().reference().child("stores").child(Session.authUser!.uid).child("promotions").observe(.value) { (snapshot) in
            
            if let dict = snapshot.value as? Dictionary<String,AnyObject>{
                var promotionArray:[Promotion] = []
                let promotions = JSON(dict)
                
                for promotionBase in promotions {
                    let promotionId = promotionBase.0
                    let promotion = promotionBase.1
                    
                    let description = promotion["description"].stringValue
                    
                    let expires = promotion["expires"].doubleValue
                    let expirationDate = Date(timeIntervalSince1970: expires)
                    let type = promotion["description"].intValue
                    
                    let promotionStruct = Promotion(id: promotionId, description: description, expirationDate: expirationDate, type: type)
                    promotionArray.append(promotionStruct)
                }
                Store.mainStore.promotions = promotionArray
                self.tableView.reloadData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Store.mainStore.promotions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionTableViewCell", for: indexPath) as! PromotionTableViewCell
        
        let promotion = Store.mainStore.promotions[indexPath.section]
        cell.setPromotion(promotion)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let promotion = Store.mainStore.promotions[indexPath.section]
            Database.database().reference().child("stores")
                .child(Session.authUser!.uid)
                .child("promotions")
                .child(promotion.id).removeValue()
            
        }
    }
    
}
