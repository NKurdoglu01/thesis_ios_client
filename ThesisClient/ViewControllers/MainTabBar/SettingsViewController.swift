//
//  SettingsViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 08/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class SettingsViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var areaField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var aptNoField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameField.text = Store.mainStore.name
        self.countryField.text = Store.mainStore.address.description.country
        self.cityField.text = Store.mainStore.address.description.city
        self.areaField.text = Store.mainStore.address.description.area
        self.streetField.text = Store.mainStore.address.description.street
        self.aptNoField.text = Store.mainStore.address.description.apartmentNo.description

        Database.database().reference().child("stores").child(Store.mainUser.uid).child("name").observe(.value) { (snapshot) in
            
            if let name = snapshot.value as? String{
                self.nameField.text = name
            }
        }
        
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").observe(.value) { (snapshot) in
            
            if let dict = snapshot.value as? Dictionary<String, AnyObject>{
                let json = JSON(dict)
                
                self.countryField.text = json["country"].stringValue
                self.cityField.text = json["city"].stringValue
                self.areaField.text = json["area"].stringValue
                self.streetField.text = json["street"].stringValue
                self.aptNoField.text = json["apartmentNo"].stringValue
            }
        }
    }
    
    @IBAction func updateAction(_ sender: Any) {
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("name").setValue(self.nameField.text)
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").child("country").setValue(self.countryField.text)
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").child("city").setValue(self.cityField.text)
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").child("area").setValue(self.areaField.text)
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").child("street").setValue(self.streetField.text)
        Database.database().reference().child("stores").child(Store.mainUser.uid).child("address").child("apartmentNo").setValue(Int(self.aptNoField.text!))

    }
    
}
