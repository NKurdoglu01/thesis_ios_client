//
//  AddressViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 05/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import SCLAlertView

class AddressViewController: UIViewController {
    
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var areaField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var apartmentNoField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alertView = SCLAlertView(appearance: appearance)
        
        if self.countryField.text!.isEmpty {
            let txt = alertView.addTextField("Enter country")
            alertView.addButton("Next") {
                self.countryField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter country", subTitle: "Please enter the country")
        }else if self.cityField.text!.isEmpty {
            let txt = alertView.addTextField("Enter city")
            alertView.addButton("Next") {
                self.cityField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter city", subTitle: "Please enter the city")
        }else if self.areaField.text!.isEmpty {
            let txt = alertView.addTextField("Enter area")
            alertView.addButton("Next") {
                self.areaField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter area", subTitle: "Please enter the area")
        }else if self.streetField.text!.isEmpty {
            let txt = alertView.addTextField("Enter street")
            alertView.addButton("Next") {
                self.streetField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter street", subTitle: "Please enter the street")
        }else if self.apartmentNoField.text!.isEmpty {
            let txt = alertView.addTextField("Enter apartment no.")
            alertView.addButton("Next") {
                self.streetField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter apartment no.", subTitle: "Please enter the apartment no.")
        }else{
            let locationVC = StoryboardScene.Login.locationViewController.instantiate()
            self.navigationController?.pushViewController(locationVC, animated: true)
        }
    }
    
}
