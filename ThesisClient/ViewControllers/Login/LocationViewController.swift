//
//  LocationViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 05/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import SCLAlertView
import GeoFire

class LocationViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    private var annotation:MKPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gestureRecognizer.delegate = self
        self.mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    @IBAction func finishAction(_ sender: Any) {
        let addressVC = (self.navigationController?.viewControllers[self.navigationController!.viewControllers.count - 2] as! AddressViewController)
        let signupVC = (self.navigationController?.viewControllers[self.navigationController!.viewControllers.count - 3] as! SignupViewController)

        
        let country = addressVC.countryField.text!
        let city = addressVC.cityField.text!
        let street = addressVC.streetField.text!
        let area = addressVC.areaField.text!
        let apartmentNo = Int(addressVC.apartmentNoField.text!)!
        
        let email = signupVC.emailField.text!
        let password = signupVC.passwordField.text!
        let storeName = signupVC.storeNameField.text!
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            
            if let err = error {
                print(err)
                let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Dismiss") {
                    alertView.dismiss(animated: true, completion: nil)
                }
                
                alertView.showError("Error", subTitle: err.localizedDescription)
                
            }else{
                let ref = Database.database().reference()
                
                let latitude = self.annotation!.coordinate.latitude
                let longitude = self.annotation!.coordinate.longitude
                
                let location = CLLocation(latitude: latitude, longitude: longitude)
                let address = Address(location: location, description: AddressDescription(apartmentNo: apartmentNo, area: area, city: city, country: country, street: street))
                
                Store.initMainStore(name: storeName, promotions: [], address: address, user: authResult!.user)
                GeoFire(firebaseRef: ref.child("locations")).setLocation(CLLocation(latitude: latitude, longitude: longitude) , forKey: authResult!.user.uid)
                ref.child("stores").child(authResult!.user.uid).setValue(
                    ["name":storeName,
                          "address":["country":country,
                                     "city":city,
                                     "area":area,
                                     "apartmentNo":apartmentNo,
                                     "street":street,
                                     "location":[self.annotation!.coordinate.latitude,
                                                 self.annotation!.coordinate.longitude],
                                     "promotions":[]]])
                
                
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        
        if let tempAnnotation = self.annotation {
            self.mapView.removeAnnotation(tempAnnotation)
        }
        
        let location = gestureReconizer.location(in: self.mapView)
        let coordinate = self.mapView.convert(location,toCoordinateFrom: self.mapView)
        
        self.annotation = MKPointAnnotation()
        self.annotation!.coordinate = coordinate
        
        self.mapView.addAnnotation(annotation!)
    }
}
