//
//  SignupViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 05/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import SCLAlertView

class SignupViewController: UIViewController {

    @IBOutlet weak var storeNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alertView = SCLAlertView(appearance: appearance)
        
        if self.storeNameField.text!.isEmpty {
            
            let txt = alertView.addTextField("Enter store name")
            alertView.addButton("Next") {
                self.storeNameField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter store name", subTitle: "Please enter the store name")
        }else if self.emailField.text!.isEmpty {
            
            let txt = alertView.addTextField("Enter e-mail")
            alertView.addButton("Next") {
                self.emailField.text = txt.text
                self.nextAction(sender)
            }
            alertView.showWarning("Enter e-mail", subTitle: "Please enter e-mail")
        }else if self.passwordField.text!.isEmpty {
            let txt = alertView.addTextField("Create password")
            alertView.addButton("Next") {
                self.passwordField.text = txt.text
                self.nextAction(sender)
            }
            
            alertView.showWarning("Create password", subTitle: "Create a password for your account")
        }else if self.repeatPasswordField.text!.isEmpty {
            let txt = alertView.addTextField("Verify password")
            alertView.addButton("Next") {
                self.repeatPasswordField.text = txt.text
                self.nextAction(sender)
            }
            
            alertView.showWarning("Verify password", subTitle: "Repeat your password to verify")
        }else if self.passwordField.text != self.repeatPasswordField.text {
            let alertView = SCLAlertView()
            alertView.showWarning("Password verification fail", subTitle: "Password you entered in the verification field is different than your password")
            
        }else{
            let addressVC = StoryboardScene.Login.addressViewController.instantiate()
            self.navigationController?.pushViewController(addressVC, animated: true)
            
        }
        
    }
    
}
