//
//  LoginViewController.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 05/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import CoreLocation

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    @IBAction func loginAction(_ sender: Any) {
        Auth.auth().signIn(withEmail: self.emailField.text!, password: self.passwordField.text!) {authDataResult, error in
            if let err = error{
                print(err)
            }else{
                Session.setEmail(self.emailField.text!)
                Session.setPassword(self.passwordField.text!)
                Session.enableAutoLogin()
                
                Session.authUser = authDataResult!.user
                
                Database.database().reference().child("stores").child(Session.authUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let dict = snapshot.value as? Dictionary<String,AnyObject>{
                        let json = JSON(dict)
                        
                        let name = json["name"].stringValue
                        let address = json["address"]
                        
                        let aptNo = address["apartmentNo"].intValue
                        let area = address["area"].stringValue
                        let city = address["city"].stringValue
                        let country = address["country"].stringValue
                        let street = address["street"].stringValue
                        
                        let location = address["location"].arrayValue
                        let latitude = location[0].doubleValue
                        let longitude = location[1].doubleValue

                        var promotionArray:[Promotion] = []
                        let promotions = json["promotions"]
                        for promotionBase in promotions {
                            let promotionId = promotionBase.0
                            let promotion = promotionBase.1
                            
                            let description = promotion["description"].stringValue
                            
                            let expires = promotion["expires"].doubleValue
                            let expirationDate = Date(timeIntervalSince1970: expires)
                            let type = promotion["description"].intValue
                            
                            let promotionStruct = Promotion(id: promotionId, description: description, expirationDate: expirationDate, type: type)
                            promotionArray.append(promotionStruct)
                        }
                        
                        let addressStruct = Address(location: CLLocation(latitude: latitude, longitude: longitude) , description: AddressDescription(apartmentNo: aptNo, area: area, city: city, country: country, street: street))
                        
                        Store.initMainStore(name: name, promotions: promotionArray, address: addressStruct, user: Session.authUser!)
                        
                        let mainTabBarVC = StoryboardScene.MainTabBar.initialScene.instantiate()
                        self.present(mainTabBarVC, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let signupVC = StoryboardScene.Login.signupViewController.instantiate()
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
}
