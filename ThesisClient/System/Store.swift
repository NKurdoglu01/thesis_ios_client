//
//  Store.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 04/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation
import Firebase
import GeoFire

class Store {
    
    private(set) static var mainStore:Store!
    private(set) static var mainUser:User!
    
    private(set) var name:String!
    public var promotions:[Promotion]!
    private(set) var address:Address!
    
    private init(name:String, promotions:[Promotion], address:Address){
        self.name = name
        self.promotions = promotions
        self.address = address
    }
    
    public static func initMainStore(name:String, promotions:[Promotion], address:Address, user:User){
        let tempStore = Store(name: name, promotions: promotions, address: address)
        self.mainStore = tempStore
        self.mainUser = user
    }
}

struct Address {
    var location:CLLocation?
    let description:AddressDescription
}

struct AddressDescription {
    let apartmentNo:Int
    let area:String
    let city:String
    let country:String
    let street:String
}

struct Promotion {
    let id:String
    let description:String
    let expirationDate:Date
    let type:Int
}
