//
//  AppDelegate.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 04/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        FirebaseApp.configure()
        VersionControl.initialize()
        GMSServices.provideAPIKey("AIzaSyA39QyqPs4QlyAj1Tcon_mbHXrsyrtGXhs")
        GMSPlacesClient.provideAPIKey("AIzaSyA39QyqPs4QlyAj1Tcon_mbHXrsyrtGXhs")
        
        let remoteConfig = RemoteConfig.remoteConfig()
        
        remoteConfig.setDefaults(fromPlist: "RemoteConfigurations")
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                remoteConfig.activateFetched()
            } else {
                print("Config not fetched")
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        VersionControl.checkVersion()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
