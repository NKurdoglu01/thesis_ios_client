//
//  Session.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 04/03/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation

import Foundation
import Firebase

class Session {
    
    public static var isAutoLoginEnabled:Bool {
        return UserDefaults.standard.bool(forKey: "isAutoLoginEnabled")
    }
    public static var email:String {
        return UserDefaults.standard.string(forKey: "mainUserEmail") ?? ""
    }
    
    public static var password:String {
        return UserDefaults.standard.string(forKey: "mainUserPassword") ?? ""
    }
    
    public static func enableAutoLogin(){
        UserDefaults.standard.set(true, forKey: "isAutoLoginEnabled")
    }
    
    public static func setEmail(_ email:String){
        UserDefaults.standard.set(email, forKey: "mainUserEmail")
    }
    public static func setPassword(_ password:String){
        UserDefaults.standard.set(password, forKey: "mainUserPassword")
    }
    
    public static var authUser:User?
    public static var authState:Auth!
    
    private static var loginStateChangeHandlers:[()->()] = []
    private static var loginStateListener:AuthStateDidChangeListenerHandle {
        return Auth.auth().addStateDidChangeListener { (auth, user) in
            Session.authState = auth
            for handler in Session.loginStateChangeHandlers {
                handler()
            }
        }
    }
    
    public static func addLoginStateChangeHandler(_ handler:@escaping ()->()){
        Session.loginStateChangeHandlers.append(handler)
    }
    
}
