//
//  VersionControl.swift
//  ThesisClient
//
//  Created by Naci Kurdoglu on 21/05/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation
import SwiftyJSON
import Firebase

class VersionControl {
    
    private(set) static var remoteConfig = RemoteConfig.remoteConfig()
    private static let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    private(set) static var storeVersion:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    private(set) static var mode:Int = 3
    private(set) static var storeUrl:URL = URL(string: "https://itunes.apple.com/app/id1449981474?mt=8")!
    
    public static func initialize(){
        self.remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        self.remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: true)
        
        VersionControl.remoteConfig.fetch(withExpirationDuration: 60) { (status, error) in
            VersionControl.remoteConfig.fetch { (status, error) in
                
                switch status {
                case .success:
                    self.remoteConfig.activateFetched()
                    VersionControl.checkVersion()
                case .noFetchYet:
                    break
                case .throttled:
                    break
                case .failure:
                    print("Config not fetched")
                    print("Error: \(error?.localizedDescription ?? "No error available.")")
                @unknown default:
                    fatalError()
                }
            }
        }
    }
    
    public static func checkVersion() {
        let json = JSON(parseJSON: VersionControl.remoteConfig["version_control_client"].stringValue!)
        
        VersionControl.storeVersion = json["version"].stringValue
        VersionControl.mode = json["mode"].intValue
        let title = json["title"].stringValue
        let message = json["message"].stringValue
        VersionControl.storeUrl = URL(string:json["appstore_url"].stringValue)!
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            if VersionControl.storeVersion.compare(self.appVersion, options: .numeric) == .orderedDescending {
                switch VersionControl.mode {
                case 1:
                    let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    alertVC.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action) in
                        UIApplication.shared.open(VersionControl.storeUrl, options:[:], completionHandler: nil)
                    }))
                    topController.present(alertVC, animated: true, completion: nil)
                case 2:
                    let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    alertVC.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action) in
                        UIApplication.shared.open(VersionControl.storeUrl, options:[:], completionHandler: nil)
                    }))
                    topController.present(alertVC, animated: true, completion: nil)
                case 3:
                    let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    topController.present(alertVC, animated: true, completion: nil)
                default:
                    break
                }
            }else if VersionControl.mode == 3 {
                let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
                topController.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
}
